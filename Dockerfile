FROM node:10.4

COPY . /src

WORKDIR /src

RUN npm install

CMD ["npm", "start"]
