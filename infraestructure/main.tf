resource "aws_ecr_repository" "todo_app2" {
  name = "todo_app_repo"
}

resource "aws_ecs_cluster" "my_cluster" {
  name = "my-cluster"
}

resource "aws_main_route_table_association" "correction" {
  vpc_id         = aws_vpc.vpc-todo_app2.id
  route_table_id = aws_route_table.rtb-todo_app2.id
}

resource "aws_subnet" "subnet-publica-todo_app2" {
  vpc_id                  = aws_vpc.vpc-todo_app2.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-1a"

  tags = {
    Name      = "Sub-Publica"
    Terraform = "True"
  }
}

resource "aws_subnet" "subnet-privada-todo_app2" {
  vpc_id                  = aws_vpc.vpc-todo_app2.id
  cidr_block              = "10.0.10.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-1a"

  tags = {
    Name      = "Sub-Privada"
    Terraform = "True"
  }
}

resource "aws_default_network_acl" "net-acl-todo_app2" {
  default_network_acl_id = aws_vpc.vpc-todo_app2.default_network_acl_id

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name      = "Net-ACL-Todo_app2"
    Terraform = "True"
  }
}

resource "aws_route_table" "rtb-todo_app2" {
  vpc_id = aws_vpc.vpc-todo_app2.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gateway-todo_app2.id
  }

  tags = {
    Name = "Router-Tab-Todo_app2"
    Terraform = "True"
  }
}

resource "aws_internet_gateway" "gateway-todo_app2" {
  vpc_id = aws_vpc.vpc-todo_app2.id

  tags = {
    Name = "GW-Todo_app2"
    Terraform = "True"
  }
}
