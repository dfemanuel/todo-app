variable "ami" {
    type = map
    default = {
        "us-east-1" = "ami-09e67e426f25ce0d7"
    }
}

variable "tipo_instancia" {
    type = string
    default = "t2.micro"
}