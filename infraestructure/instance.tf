resource "aws_instance" "web" {
  ami           = var.ami["us-east-1"]
  instance_type = var.tipo_instancia
  key_name      = "chave-Todo_app2"
  subnet_id     = aws_subnet.subnet-publica-todo_app2.id
  vpc_security_group_ids = [
    aws_security_group.in-http.id,
    aws_security_group.out-all.id,
    aws_security_group.ssh-in-devops.id
  ]
  tags = {
    Name = "SRV-Web"
    Terraform = "True"
  }
  depends_on = [aws_s3_bucket.script-todo_app2]
  user_data = templatefile("scripts/instala-web.sh.tpl", { ip_mongo = aws_instance.db.private_ip })
}

resource "aws_instance" "db" {
  ami           = var.ami["us-east-1"]
  instance_type = var.tipo_instancia
  key_name      = "chave-PosUni7"
  subnet_id     = aws_subnet.subnet-privada-todo_app2.id
  vpc_security_group_ids = [
    aws_security_group.pass-db.id,
    aws_security_group.out-all.id,
    aws_security_group.ssh-in-local.id
  ]
  tags = {
    Name = "SRV-Database"
    Terraform = "True"
  }
  depends_on = [aws_s3_bucket.script-todo_app2]
  user_data = file("scripts/instala-db.sh")
}
