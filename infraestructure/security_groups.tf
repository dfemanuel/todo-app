resource "aws_security_group" "ssh-in-devops" {
  name          = "ssh-in-devops"
  vpc_id        = aws_vpc.vpc-todo_app2.id
  # description   = "Regra de acesso para devops"
  ingress {
    description = "SSH administrativo"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    # cidr_blocks = ["0.0.0.0/0"]
    cidr_blocks = ["177.42.157.155/32"]
  }
  tags = {
    Name        = "ssh-in-devops"
    Terraform   = "true"
  }
}

resource "aws_security_group" "ssh-in-local" {
  name          = "ssh-in-local"
  vpc_id        = aws_vpc.vpc-todo_app2.id
  # description   = "Regra de acesso para devops"
  ingress {
    description = "SSH administrativo"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = "ssh-in-local"
    Terraform   = "true"
  }
}

resource "aws_security_group" "out-all" {
  name          = "out-all"
  vpc_id        = aws_vpc.vpc-todo_app2.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = "out-all"
    Terraform   = "true"
  }
}

resource "aws_security_group" "in-http" {
  name          = "in-http"
  vpc_id        = aws_vpc.vpc-todo_app2.id
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = "in-http"
    Terraform   = "true"
  }
}

resource "aws_security_group" "pass-db" {
  name          = "pass-db"
  vpc_id        = aws_vpc.vpc-posuni7.id
  ingress {
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_groups = [aws_security_group.in-http.id]
  }
  tags = {
    Name        = "pass-db"
    Terraform   = "true"
  }
}
